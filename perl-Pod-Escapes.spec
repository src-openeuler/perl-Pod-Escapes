Name:           perl-Pod-Escapes
Epoch:          1
Version:        1.07_01
Release:        2
Summary:        Resolve POD escape sequences
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/Pod-Escapes
Source0:        https://cpan.metacpan.org/authors/id/N/NE/NEILB/Pod-Escapes-%{version}.tar.gz

BuildArch:      noarch

BuildRequires:  coreutils findutils perl-interpreter perl-generators perl(ExtUtils::MakeMaker)

#for test
BuildRequires:  perl(Test)  perl(utf8)

%description
This module provides things that are useful in decoding Pod E<...>
sequences. Presumably, it should be used only by Pod parsers and/or
formatters.

By default, Pod::Escapes exports none of its symbols. But you can request
any of them to be exported. Either request them individually, as with `use
Pod::Escapes qw(symbolname symbolname2...);', or you can do `use
Pod::Escapes qw(:ALL);' to get all exportable symbols.

%package_help

%prep
%autosetup -n Pod-Escapes-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PERLLOCAL=1 NO_PACKLIST=1
%make_build

%install
%make_install
%{_fixperms} %{buildroot}

%check
make test

%files
%{perl_vendorlib}/Pod/*

%files help
%doc Changes README
%{_mandir}/man3/*

%changelog
* Sun Jan 19 2025 Funda Wang <fundawang@yeah.net> - 1:1.07_01-2
- drop useless perl(:MODULE_COMPAT) requirement

* Thu Oct 20 2022 huyubiao <huyubiao@huawei.com> - 1:1.07_01-1
- Upgrade version to 1.07_01

* Thu Jul 22 2021 Jiuyue Ma <majiuyue@huawei.com> - 1:1.07-420
- fix conflict package file '%{perl_vendorlib}/Pod'

* Sat Oct 26 2019 yefei <yefei25@huawei.com> - 1:1.07-419
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: move README to help folder

* Wed Aug 28 2019 openEuler Buildteam <buildteam@openeuler.org> - 1:1.07-418
- Package init
